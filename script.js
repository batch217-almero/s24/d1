//console.log("Test");

// EXPONENT OPERATOR ------------------------------------------------------
// OLD METHOD
const firstNum = 8 ** 2;
console.log(firstNum);

// AFTER ES6
const secondNum = Math.pow(8, 2);
console.log(secondNum);

const thirdNum = Math.pow(8, 3);
console.log(thirdNum);


// TEMPLATE LITERALS -----------------------------------------------------
let name = "Donny";
// OLD METHOD
let message = 'Hello ' + name + '! Welcome to programming!';
console.log("Message without template literals: " + message);

// USING TEMPLATE LITERAL
message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with literals: ${message}`);

// MULTILINE 
const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8 ** 2 with solution of ${firstNum}
`;
console.log(anotherMessage);

console.log(`The interest rate is: ${1000 * .1}`);


// ARRAY DESTRUCTURING ------------------------------------------------
const fullName = ["Juan", "Dela", "Cruz"];

// OLD METHOD
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// USING MIXED OLD AND NEW TEMPLATE LITERAL
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet yah!`);

// USING NEW METHOD TO DIVIDE ARRAY INTO INDIVIDUAL VARIABLES
const [firstName, middleName, lastName] = fullName;

console.log(`Hello ${firstName} ${middleName} ${lastName}`);


// OBJECT DESTRUCTURING ---------------------------------------------
const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
};

// OLD METHOD
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you.`); 	

// NEW METHOD
const {givenName, maidenName, familyName} = person;
console.log(`Hello ${givenName} ${maidenName} ${familyName}`);

function getFullName({givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person);


// ARROW FUNCTION ( => )--------------------------------------------------------
const hello = () => {
	console.log("Hello World!")
}

hello();

// OLD METHOD
function printFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}

printFullName("John", "Doe", "Smith");

// USING ARROW FUNCTION
const printFullName2 = (firstName, middleName, lastName) => {
	console.log(`${firstName} ${middleName} ${lastName}`);
}

printFullName2("John", "Doe", "Smith");

// ARROW FUNCTION with LOOPS
const students = ["John", "Jane", "Judy"];

students.forEach(function(student){
	console.log(`${student} is a student`);
});

// ARROW FUNCTION
students.forEach((student) => {
	console.log(`${student} is a student`);
})

// DEFAULT FUNCTION ARGUMENT VALUE -
//If walang argument, ggamitin ni fuction yung default argument
const greet = (name = "User") => { //"User" yung default function argument
	return `Good morning, ${name}`;
}

console.log(greet());
console.log(greet("Donny"));
console.log(greet());

// CLASS-BASED OBJECT BLUEPRINTS
class Car {
	constructor(brand, name, year){
		this.brand = brand,
		this.name = name,
		this.year = year
	}
}

const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);